
(define rename-region (lambda(orginalmatname newmatname rname)
	(sde:separate-lumps)
	(define j 0)
	(define ll (find-material-id orginalmatname))
	(if (not (list? ll)) (set! ll (list ll)) )
	(if (not (equal? ll '())) (begin
			(for-each (lambda(reg)
				(sde:add-material reg newmatname (string-append rname "_" (number->string j)))
				(set! j (+ j 1))
				)
			ll
			)		
	))
(display "Nombre de cuboids de ")(display newmatname)(display ": ")(display (length ll))(newline)
(display (edge:shortest (entity:edges (get-body-list))))(newline)
))



(define load-icwb-layout (lambda(file simdomain)
	(sdeicwb:load-file file 0.001)
	(define layersname (sdeicwb:get-layer-names))
	(define layersID (sdeicwb:get-layer-ids))
	(sdeicwb:set-domain simdomain)
		(for-each (lambda(layer)
			(sdeicwb:generate-mask-by-layer-name layer layer)
			)
		layersname
		)
	layersname
	)
)





(define read-layout-init-domain (lambda(file type domain)
(if type (begin
	(sdeio:read_dfise_mask file)

	(define bbox-device (bbox (find-mask domain)))

	(define maxdx (position:x (cdr bbox-device)))
	(define mindx (position:x (car bbox-device)))
	(define maxdy (position:y (cdr bbox-device)))
	(define mindy (position:y (car bbox-device)))
)
(begin
	(load-icwb-layout file domain)

	(define mindx (sdeicwb:get-xmin))
	(define maxdx (sdeicwb:get-xmax))
	(define mindy (sdeicwb:get-ymin))
	(define maxdy (sdeicwb:get-ymax))
	
	(define maskmindx (sdeicwb:get-left))
	(define maskmaxdx (sdeicwb:get-right))
	(define maskmaxdy (sdeicwb:get-front))
	(define maskmindy (sdeicwb:get-back))
	
	(sdegeo:set-default-boolean "XX")	
	(sdegeo:translate-selected (get-mask-list) (transform:translation (gvector (- maxdx maskmaxdx) (- maxdy maskmaxdy) 0)) #f 0)
	(sdegeo:set-default-boolean "ABA")	

)
)




(sdepe:define-pe-domain (list mindx   mindy  maxdx   maxdy) )

))



;;(refinement:set-prop sde-ref "grid mode"  "AF_GRID_ONE_DIR")

 
(define fill (lambda(mat h)

(sdegeo:set-default-boolean "BAB")

	(define gbbox (bbox (get-body-list)))

		(define bmaxx (position:x (cdr gbbox)))
		(define bminx (position:x (car gbbox)))
		(define bmaxy (position:y (cdr gbbox)))
		(define bminy (position:y (car gbbox)))
		(define bmaxz (position:z (cdr gbbox)))
		(define bminz (position:z (car gbbox)))
	(sdegeo:create-cuboid (position bminx bminy bminz) (position bmaxx bmaxy (+ h bmaxz)) mat "rmat")
	
(sdegeo:set-default-boolean "ABA")

))

(define intersect-mask (lambda(mask1 mask2 maskresult)
	;;;bool:nonreg-intersect
	(define res (bool:nonreg-intersect (list (entity:copy (find-mask mask1)) (entity:copy (find-mask mask2)) ) ) )
	(sde:attrib-remove res "maskname")
	(generic:add res "maskname" maskresult)
	(sde:list-generic-attribs res)
	)
)

(define subtract-mask (lambda(mask1 mask2 maskresult)
	;;;bool:nonreg-intersect
	(define res (bool:subtract (list (entity:copy (find-mask mask1)) (entity:copy (find-mask mask2)) ) ) )
	(sde:attrib-remove res "maskname")
	(generic:add res "maskname" maskresult)
	(sde:list-generic-attribs res)
	)
)

(define place-contact-mask (lambda(contact mask polar)

	;;(define patbody (sdepe:pattern "mask" mask "material" "tmp" "thickness" 0.1 "polarity" polar ))
	;;(if (not (null? (entity:check patbody 70))) (entity:delete patbody))
	(sdepe:pattern "mask" mask "material" "tmp" "thickness" 0.2 "polarity" polar "algorithm" "sweep" "type" "aniso")
	(sdegeo:define-contact-set contact 4  (color:rgb 1 0 0 ) "##" )
	(sdegeo:set-current-contact-set  contact)
	(if (not (null? (find-material-id "tmp"))) (sdegeo:set-contact-boundary-faces (find-material-id "tmp") ))
	(entity:delete (find-material-id "tmp") )
))

(define place-doping-profile (lambda(dopingdef mask polar material)

	(sdepe:pattern "mask" mask "material" "Resist" "thickness" 1.0 "polarity" polar )
	(define ff (bool:nonreg-intersect (entity:copy (find-material-id material)) (entity:copy (find-material-id "Resist")) ) )

	(define list-refeval (list))
	(define faces (entity:faces ff))
	(define ii 1)
	(if (> (length faces) 1)
		(begin 
			(for-each (lambda(face)
				(set! list-refeval (append list-refeval (extract-refwindow face (string-append  dopingdef "_refeval_" (number->string ii)))))
			(set! ii (+ ii 1))
				)
			faces
			)
		)
		(begin
			(define list-refeval (extract-refwindow faces (string-append  dopingdef "_refeval_1" )))
		)
	)	
	
	(define j 1)
	(for-each (lambda(place)
		(sdedr:define-analytical-profile-placement
			(string-append  "AnalyticalProfilePlacement_" dopingdef "_" (number->string j))
			dopingdef 
			(string-append  dopingdef "_refeval_" (number->string j))
			"Both" "NoReplace" "Eval" material 0 "material" 
		)
		(set! j (+ j 1))
		)
	list-refeval
	)
	(entity:delete (find-material-id "Resist"))
	(entity:delete ff)
	(map bool:regularise (list (find-material-id material)))
	list-refeval
))

(define refeval-mask (lambda(name mask type ztop zbot xyoffset max_x_ref max_y_ref max_z_ref min_x_ref min_y_ref min_z_ref deflist)
	
;;(find-mask "CHANX")
;;(define mas (entity:copy (find-mask "CHANX")))
;;(define listbody (body:separate mas))

	(define h (find-mask mask))	
	(define mas (entity:copy (find-mask mask)))
	(define listbody (body:separate mas))
	(define ii 1)
	
	(sdedr:define-refinement-size (string-append  "RefDef_" mask "_" name)  max_x_ref max_y_ref max_z_ref min_x_ref min_y_ref min_z_ref )
	
	(if (> (length deflist) 0)
		(for-each (lambda(def)	(eval (append (list 'sdedr:define-refinement-function) (list (string-append  "RefDef_" mask "_" name)) def )) ) deflist )
	)
	
	(for-each (lambda(body)	
		(if (equal? type "poly")
			(begin
		(define vlist (entity:vertices body))
		(define positionlist (list))
		(for-each (lambda(v) (set! positionlist (append positionlist (list (vertex:position v))) )) vlist)
		
	
		(sdedr:define-refeval-window (string-append  "RefEvalWin_" mask "_" name "_" (number->string ii)) "Polygon" positionlist)
	
		(define lopt (lop:options))
		(lop:options "repair_self_intersections" #t "check_remote_face_face_intersections" #t "add_vent_faces" #t lopt)

		(lop:extend-sheet(entity:edges (find-drs-id (string-append  "RefEvalWin_" mask "_" name "_" (number->string ii)))) xyoffset lopt)
		(sdegeo:translate-selected (find-drs-id (string-append  "RefEvalWin_" mask "_"  name "_" (number->string ii))) (transform:translation (gvector 0 0 zbot)) #f 0)
		(sdegeo:sweep (find-drs-id (string-append  "RefEvalWin_" mask "_" name "_" (number->string ii)))  (- ztop zbot)  )
		)
			(begin
			(define garea (bbox body))

		(define bmaxx (position:x (cdr garea)))
		(define bminx (position:x (car garea)))
		(define bmaxy (position:y (cdr garea)))
		(define bminy (position:y (car garea)))
		
	(sdedr:define-refeval-window (string-append  "RefEvalWin_" mask "_" name "_" (number->string ii)) "Cuboid" 
		(position (- bminx xyoffset) (- bminy xyoffset) ztop ) (position (+ bmaxx xyoffset) (+ bmaxy xyoffset) zbot))
	
			)
		)
	
	(sdedr:define-refinement-placement 
		(string-append  "RefPlace_" mask "_" name "_" (number->string ii)) 
		(string-append  "RefDef_" mask "_" name )
		(string-append  "RefEvalWin_" mask "_" name "_" (number->string ii)) )

	(set! ii (+ ii 1))
		)
	listbody
	)
	;;(entity:delete mas)
	(entity:delete listbody)
))


(define fillet-selected-external-edges-at-z (lambda(mat zeq radius)
	(sdepe:fill-device "material" "tmp" "height" zeq)
	(sde:add-material (entity:copy (find-material-id mat)) "tmp2" "dd")
	(define edge-list (entity:edges (bool:nonreg-intersect (list (find-material-id "tmp2") (find-material-id "tmp"))) ))

	(define llist (list))
   	(for-each (lambda(edge)
	       (define z1 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define z2 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (if  (and (< (abs (- z1 zeq)) 1e-8) (< (abs (- z2 zeq)) 1e-8)  )
	       	   (set! llist (append llist (list edge)))
		   ) 
	    )
	    edge-list )


	(sdegeo:bool-unite (list (find-material-id "tmp2" ) (find-material-id mat )))

	(sdegeo:fillet-edges llist  radius )
	(sde:add-material (find-material-id "tmp2") mat "tmp")
))






(define findedges-plan  (lambda (zeq mat)
   (define leelist (list))

(define resabs 1e-10)   

(define xmin (position:x (car (bbox (get-body-list) ) ) ) )
(define xmax (position:x (cdr (bbox (get-body-list) ) ) ) )
(define ymin (position:y (car (bbox (get-body-list) ) ) ) )
(define ymax (position:y (cdr (bbox (get-body-list) ) ) ) )

   (define dede (entity:edges (find-material-id mat)) )
   (for-each (lambda(edge)
	       (define dedev (entity:vertices edge))
	       (define nmax (length dedev)) 
	       (define xmm 0)
	       (define ymm 0)
	       (define zmm 0)
	       (define xmmm 0)
	       (define ymmm 0)
	       (define zmmm 0)
	       (define tmplist (list))

	       (for-each (lambda(vertex)
			   (define xc (position:x (vertex:position vertex)) ) 
			   (define yc (position:y (vertex:position vertex)) ) 
			   (define zc (position:z (vertex:position vertex)) )
			   (if  (< (abs (- xc xmin)) resabs) (set! xmm (+ xmm 1)))
			   (if  (< (abs (- yc ymin)) resabs) (set! ymm (+ ymm 1)))
			   (if  (< (abs (- xc xmax)) resabs) (set! xmmm (+ xmmm 1)))
			   (if  (< (abs (- yc ymax)) resabs) (set! ymmm (+ ymmm 1)))
			   ) dedev)
	       	       
	       (if  (and (> nmax xmm) (> nmax ymm) (> nmax xmmm) (> nmax ymmm) ) 
		   	(begin
			    (for-each (lambda(vertex)
					(define zc (position:z (vertex:position vertex)) )
					(if (< (abs (- zc zeq)) resabs) (set! zmm (+ zmm 1)))
			   ) dedev)

			   (if  (< (abs (- nmax zmm)) resabs)
		   	   (set! leelist (append leelist (list edge))) )
			   ) 
		) 
	    )
	    dede )
	    leelist)
leelist
)











(define filletedges-plan  (lambda (zeq mat r)

   (define leelist (list))

(define resabs 1e-10)   

(define xmin (position:x (car (bbox (get-body-list) ) ) ) )
(define xmax (position:x (cdr (bbox (get-body-list) ) ) ) )
(define ymin (position:y (car (bbox (get-body-list) ) ) ) )
(define ymax (position:y (cdr (bbox (get-body-list) ) ) ) )

   (define dede (entity:edges (find-material-id mat)) )
   (for-each (lambda(edge)
	       (define dedev (entity:vertices edge))
	       (define nmax (length dedev)) 
	       (define xmm 0)
	       (define ymm 0)
	       (define zmm 0)
	       (define xmmm 0)
	       (define ymmm 0)
	       (define zmmm 0)
	       (define tmplist (list))

	       (for-each (lambda(vertex)
			   (define xc (position:x (vertex:position vertex)) ) 
			   (define yc (position:y (vertex:position vertex)) ) 
			   (define zc (position:z (vertex:position vertex)) )
			   (if  (< (abs (- xc xmin)) resabs) (set! xmm (+ xmm 1)))
			   (if  (< (abs (- yc ymin)) resabs) (set! ymm (+ ymm 1)))
			   (if  (< (abs (- xc xmax)) resabs) (set! xmmm (+ xmmm 1)))
			   (if  (< (abs (- yc ymax)) resabs) (set! ymmm (+ ymmm 1)))
			   ) dedev)
	       	       
	       (if  (and (> nmax xmm) (> nmax ymm) (> nmax xmmm) (> nmax ymmm) ) 
		   	(begin
			    (for-each (lambda(vertex)
					(define zc (position:z (vertex:position vertex)) )
					(if (< (abs (- zc zeq)) resabs) (set! zmm (+ zmm 1)))
			   ) dedev)

			   (if  (< (abs (- nmax zmm)) resabs)
		   	   (set! leelist (append leelist (list edge))) )
			   ) 
		) 
	    )
	    dede )
	    (if (not (null? leelist))(sdegeo:fillet-edges leelist r) )
	    leelist)
leelist
)
 







(define fillet-edges-crossing  (lambda (zeq mat r cvx)

(define resabs 1e-10)

;;(define check-con (lambda(edge)
;;		    (if (string=? cvx "convex")
;;(cvty:convex (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01))
;;(cvty:concave (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01)) )))

	(sdepe:fill-device "material" "tmp" )
	(sde:add-material (entity:copy (find-material-id mat)) "tmp2" "dd")
	(define edge-list (entity:edges (bool:nonreg-intersect (list (find-material-id "tmp2") (find-material-id "tmp"))) ))

   (define leelist (list))
   
(define xmin (position:x (car (tight-bbox (find-material-id mat ) ) ) ) )
(define xmax (position:x (car (cdr (tight-bbox (find-material-id mat ) ) ) ) ) )
(define ymin (position:y (car (tight-bbox (find-material-id mat ) ) ) ) )
(define ymax (position:y (car (cdr (tight-bbox (find-material-id mat ) ) ) ) ) )


   (for-each (lambda(edge)
	       (define z1 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define z2 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (define y1 (* 1.0 (position:y (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define y2 (* 1.0 (position:y (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (define x1 (* 1.0 (position:x (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define x2 (* 1.0 (position:x (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (if  (and    (< (* (- z1 zeq) (- z2 zeq)) resabs) 
				 	(and (and (> (abs (- x1 xmin)) resabs)
						 (> (abs (- x2 xmin)) resabs)  
						 (> (abs (- x1 xmax)) resabs)
						 (> (abs (- x2 xmax)) resabs) ) 
					    (and (> (abs (- y1 ymin)) resabs)
						 (> (abs (- y2 ymin)) resabs)  
						 (> (abs (- y1 ymax)) resabs)
						 (> (abs (- y2 ymax)) resabs)))    	       			
			   (string=? (edge:convexity edge) cvx)
			    )
		   	(set! leelist (append leelist (list edge)))
		   ) 
	    )
	    edge-list )

	(sdegeo:bool-unite (list (find-material-id "tmp2" ) (find-material-id mat )))

	(sdegeo:fillet-edges leelist  r )
	(sde:add-material (find-material-id "tmp2") mat "tmp")

	    )
)

(define chamfer-edges-crossing  (lambda (zeq mat r cvx )

;;(define check-con (lambda(edge)
;;		    (if (string=? cvx "convex")
;;(cvty:convex (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01))
;;(cvty:concave (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01)) )))
(define resabs 1e-10)

	(sdepe:fill-device "material" "tmp" )
	(sde:add-material (entity:copy (find-material-id mat)) "tmp2")
	(define edge-list (entity:edges (bool:nonreg-intersect (list (find-material-id "tmp2") (find-material-id "tmp"))) ))

   (define leelist (list))
   (define llist (list))
   
(define xmin (position:x (car (tight-bbox (find-material-id mat ) ) ) ) )
(define xmax (position:x (car (cdr (tight-bbox (find-material-id mat ) ) ) ) ) )
(define ymin (position:y (car (tight-bbox (find-material-id mat ) ) ) ) )
(define ymax (position:y (car (cdr (tight-bbox (find-material-id mat ) ) ) ) ) )


   (for-each (lambda(edge)
	       (define z1 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define z2 (* 1.0 (position:z (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (define y1 (* 1.0 (position:y (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define y2 (* 1.0 (position:y (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (define x1 (* 1.0 (position:x (vertex:position (list-ref (entity:vertices edge) 0)))) )
	       (define x2 (* 1.0 (position:x (vertex:position (list-ref (entity:vertices edge) 1)))) )
	       (if  (and    (< (* (- z1 zeq) (- z2 zeq)) resabs) 
				 	(and (and (> (abs (- x1 xmin)) resabs)
						 (> (abs (- x2 xmin)) resabs)  
						 (> (abs (- x1 xmax)) resabs)
						 (> (abs (- x2 xmax)) resabs) ) 
					    (and (> (abs (- y1 ymin)) resabs)
						 (> (abs (- y2 ymin)) resabs)  
						 (> (abs (- y1 ymax)) resabs)
						 (> (abs (- y2 ymax)) resabs)))    	       			
			   (string=? (edge:convexity edge) cvx)
			    )
		   	(set! leelist (append leelist (list edge)))
		   ) 
	    )
	    edge-list )

   (for-each (lambda(edge)
	       (for-each (lambda(ee)
			   (if (equal? (edge:mid-point edge) (edge:mid-point ee)) (set! llist (append llist (list ee))) ) 
			   )
		(entity:edges (find-material-id mat))	 )
	       )
   leelist)


	;;(sdegeo:bool-unite (list (find-material-id "tmp2" ) (find-material-id mat )))

	(if (> (length llist) 0) (sdegeo:chamfer llist  r r ))
	;;(sde:add-material (find-material-id "tmp2") mat )
	(entity:delete (find-material-id "tmp2") )
	
	    )
)



(define filletedges-external-edges  (lambda (mat r cvx)

;;;(define check-con (lambda(edge)
;;;		    (if (string=? cvx "convex")
;;;(cvty:convex (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01))
;;;(cvty:concave (ed-cvty-info:instantiate (edge:ed-cvty-info edge) 0.01)) )))

	(sdepe:fill-device "material" "tmp" "height" (+ (position:z (cdr (bbox (find-material-id mat)))) 0.1)    )
	(sde:add-material (entity:copy (find-material-id mat)) "tmp2" "dd")
	(define edge-list (entity:edges (bool:nonreg-intersect (list (find-material-id "tmp2") (find-material-id "tmp"))) ))

   (define leelist (list))
   

   (for-each (lambda(edge)
	       (if     (string=? (edge:convexity edge) cvx)
		   	(set! leelist (append leelist (list edge)))
		   ) 
	    )
	    edge-list )

	(sdegeo:bool-unite (list (find-material-id "tmp2" ) (find-material-id mat )))

	(sdegeo:fillet-edges leelist  r )
	(sde:add-material (find-material-id "tmp2") mat "tmp")

	    )
)





(define taperfaces  (lambda (zeq angle mat )
   
   (define leelist (list))
   (define zmax -1e10)
   (define resabs 1e-8)

(define xmin (position:x (car (bbox (get-body-list) ) ) ) )
(define xmax (position:x (cdr (bbox (get-body-list) ) ) ) )
(define ymin (position:y (car (bbox (get-body-list) ) ) ) )
(define ymax (position:y (cdr (bbox (get-body-list) ) ) ) )

   (define dede (entity:faces (find-material-id mat)) )
   (for-each (lambda(face)
	       (define dedev (entity:vertices face))
	       (define nmax (length dedev)) 
	       (define xmm 0)
	       (define ymm 0)
	       (define zmm 0)
	       (define xmmm 0)
	       (define ymmm 0)
	       (define zmmm 0)
	       (define tmplist (list))

	       (for-each (lambda(vertex)
			   (define xc (position:x (vertex:position vertex)) ) 
			   (define yc (position:y (vertex:position vertex)) ) 
			   (define zc (position:z (vertex:position vertex)) )
			   (if  (< (abs (- xc xmin)) resabs) (set! xmm (+ xmm 1)))
			   (if  (< (abs (- yc ymin)) resabs) (set! ymm (+ ymm 1)))
			   (if  (< (abs (- xc xmax)) resabs) (set! xmmm (+ xmmm 1)))
			   (if  (< (abs (- yc ymax)) resabs) (set! ymmm (+ ymmm 1)))
			   ) dedev)
	       	       
	       (if  (and (> nmax xmm) (> nmax ymm) (> nmax xmmm) (> nmax ymmm) ) 
		   	(begin
			    (for-each (lambda(vertex)
					(define zc (position:z (vertex:position vertex)) )
					(if (> zc zeq) (set! zmm (+ zmm 1)))
					(if (< zc zeq) (set! zmmm (+ zmmm 1)))
					(if (> zc zmax) (set! zmax zc))
			   ) dedev)

			   (if  (and (> nmax zmm) (> nmax zmmm) )
		   	   (set! leelist (append leelist (list face))) )
			   ) 
		) 
	    )
	    dede )
	    (sdegeo:taper-faces leelist (position 0.0  0.0  zmax) (gvector 0 0 1) angle)
	    )
)







(define findfaces-plan-zmax  (lambda (mat)
   (define leelist (list))

(define resabs 1e-10)   

(define zmax (position:z (cdr (bbox (find-material-id mat) ) ) ) )

   (define dede (entity:faces (find-material-id mat)) )
   (for-each (lambda(face)
	       (define dedev (entity:vertices face))
	       (define nmax (length dedev)) 
	       (define xmm 0)
	       (define ymm 0)
	       (define zmm 0)
	       (define xmmm 0)
	       (define ymmm 0)
	       (define zmmm 0)
	       (define tmplist (list))

	       (for-each (lambda(vertex)
			   (define zc (position:z (vertex:position vertex)) )
			   (if (< (abs (- zc zmax)) resabs) (set! zmmm (+ zmmm 1))
			   ))
		dedev)

		(if  (< (abs (- nmax zmmm)) resabs )
		   	 (set! leelist (append leelist (list face))) 
		  ) 
	)	 
      dede )
      leelist)
leelist
)



(define findfaces-plan-zmin  (lambda (mat)
   (define leelist (list))

(define resabs 1e-10)   

(define zmin (position:z (car (bbox (find-material-id mat) ) ) ) )

   (define dede (entity:faces (find-material-id mat)) )
   (for-each (lambda(face)
	       (define dedev (entity:vertices face))
	       (define nmax (length dedev)) 
	       (define xmm 0)
	       (define ymm 0)
	       (define zmm 0)
	       (define xmmm 0)
	       (define ymmm 0)
	       (define zmmm 0)
	       (define tmplist (list))

	       (for-each (lambda(vertex)
			   (define zc (position:z (vertex:position vertex)) )
			   (if (< (abs (- zc zmin)) resabs) (set! zmmm (+ zmmm 1))
			   ))
		dedev)

		(if  (< (abs (- nmax zmmm)) resabs )
		   	 (set! leelist (append leelist (list face))) 
		  ) 
	)	 
      dede )
      leelist)
leelist
)







;;;;;;(side "back" #t #f 1 0 )

(define side (let ((STATUS "front"))  (lambda(TBP flag-mask flag-clean x y )
(if (not (string=? TBP STATUS)) (begin
	(sdepe:fill-device "material" "tmp" "region" TBP )
	(sdegeo:set-default-boolean "XX")

  	(define xmin (position:x (car (tight-bbox (get-body-list) ) ) ) ) 
  	(define xmax (position:x (car (cdr (tight-bbox (get-body-list) ) ) ) ) )
  	(define ymin (position:y (car (tight-bbox (get-body-list) ) ) ) )
  	(define ymax (position:y (car (cdr (tight-bbox (get-body-list) ) ) ) ) )
  	(define zmin (position:z (car (tight-bbox (get-body-list) ) ) ) )
  	(define zmax (position:z (car (cdr (tight-bbox (get-body-list) ) ) ) ) )


	(sdegeo:mirror-selected (append (get-body-list) (get-drs-list)) 
			(transform:reflection  (position (/ (+ xmax xmin) 2.0) (/ (+ ymax ymin) 2.0) (/ (+ zmax zmin) 2.0) ) (gvector 0 0 1) )  #f)

	(if flag-mask  
		(sdegeo:mirror-selected  (get-mask-list) 
			(transform:reflection (position (/ (+ xmax xmin) 2.0) (/ (+ ymax ymin) 2.0) 0.0 ) (gvector x y 0) ) #f)
		 )
	
	(if (string=? TBP "back")
		(sdegeo:delete-region (find-region-id "front" ))
		(sdegeo:delete-region (find-region-id "back" ))
	)
	(if flag-clean (sdegeo:delete-region (find-material-id "tmp" )) ) 
))
(set! STATUS TBP)
))
STATUS
)





(define sweep-profile-mask (lambda(mask refeval1 refeval2)

	(define h (find-mask mask))	
	(define mas (entity:copy (find-mask mask)))
	(define listbody (body:separate mas))


(sdegeo:set-default-boolean "XX")

(for-each (lambda(block1)

(define edgelist (entity:edges block1))

(define bbox-device (bbox block1))

(define maxx_block1 (position:x (cdr bbox-device)))
(define minx_block1 (position:x (car bbox-device)))
(define maxy_block1 (position:y (cdr bbox-device)))
(define miny_block1 (position:y (car bbox-device)))


(define findedges-2d-xmin  (lambda (block2d)
   	(define leelist (list))
	(define resabs 1e-10)   

(define xmin (position:x (car (bbox block2d ) ) ) )
(define xmax (position:x (cdr (bbox block2d  ) ) ) )
(define ymin (position:y (car (bbox block2d  ) ) ) )
(define ymax (position:y (cdr (bbox block2d  ) ) ) )

   (define dede (entity:edges block2d) )
   (for-each (lambda(edge)
			   
	(define x1 (position:x (vertex:position (list-ref (entity:vertices edge) 0)      )) ) 
	(define x2 (position:x (vertex:position (list-ref (entity:vertices edge) 1)       )) ) 
	(if  (and (< (abs (- x1 xmin)) resabs) (< (abs (- x2 xmin)) resabs)) (set! leelist (append leelist (list edge))) )
	       	       
	    )
   dede )
leelist
))

(define edge1 (car (findedges-2d-xmin block1)))
	(define x1 (position:x (vertex:position (list-ref (entity:vertices edge1) 0)      )) ) 
	(define x2 (position:x (vertex:position (list-ref (entity:vertices edge1) 1)       )) ) 
	(define y1 (position:y (vertex:position (list-ref (entity:vertices edge1) 0)      )) ) 
	(define y2 (position:y (vertex:position (list-ref (entity:vertices edge1) 1)       )) ) 


(define vertex1 (sdegeo:insert-vertex (position x1 (+ y1 (/ (- y2 y1) 3.0)) 0)    ))
(define vertex2 (sdegeo:insert-vertex (position x1 (+ y1 (/ (* 2.0 (- y2 y1)) 3.0)) 0)    ))
(define ll (entity:edges block1))
(define midedge (list))

(for-each (lambda(edge)
	(if (equal? (edge:mid-point edge) (position x1 (/ (+ y2 y1) 2.0) 0))
		(set! midedge edge)
	)
	)
ll
)

(display "midedge=")(display midedge)(display "\n")


(define xpos1 (position:x (vertex:position (list-ref (entity:vertices midedge) 0))))
(define ypos1 (position:y (vertex:position (list-ref (entity:vertices midedge) 0))))
(define xpos2 (position:x (vertex:position (list-ref (entity:vertices midedge) 1))))
(define ypos2 (position:y (vertex:position (list-ref (entity:vertices midedge) 1))))


(define totaledgelist (entity:edges block1))
(define list1 (list))
(for-each (lambda(edge)
	(if (not (equal? edge midedge))
		(set! list1 (append list1 (list edge)))
	)
	)
totaledgelist
)

(define path1 (wire-body list1))
(define path2 (wire-body midedge))

(sdedr:define-refeval-window refeval1 "Rectangle"  (position 0.1 ypos2 5) (position 1.0 ypos2 0))
(sdedr:define-refeval-window refeval2 "Rectangle"  (position 0.1 ypos1 5) (position 1.0 ypos1 0))

(sdegeo:sweep (find-drs-id refeval1) path1  (sweep:options "solid" #t "rigid" #f "miter_type" "default"  ))
(sdegeo:sweep (find-drs-id refeval2) path2  (sweep:options "solid" #t "rigid" #f "miter_type" "default"  ))

	)
listbody
)

))



(define protect-all-contacts
 (lambda ( )
   (for-each
    (lambda (ledge )
      (if (not (equal? (sde:attrib-get-int ledge "2d-contact" ) "" ) )
      (edge:set-no-merge-attrib ledge ) )
      )
    (entity:edges (get-body-list ) ) )

   (for-each
    (lambda (lface)
      (define lelist (entity:edges lface ) )

      (if (not (equal? (sde:attrib-get-int lface "3d-contact" ) "" ) )
      (begin
        (for-each
         (lambda (ledge )
       (edge:set-no-merge-attrib ledge )
       )
         lelist )
        )
      )
      )
    (entity:faces (get-body-list ) ) )
   )
 )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Andrei Lebedev
;; Synopsys Switzerland LLC
;; mailto:alebedev@synopsys.com
;;
;; Some useful functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This function removes all elements E from the list L
;; such as myF(E)=bool
(define (filter-list L myF bool)
  (cond ((null? L) '())
	((eqv? (myF(car L)) bool)
	 (filter-list (cdr L) myF bool))
	(else (cons (car L) (filter-list (cdr L) myF bool)))))


;; Calculates vector product of two gvectors
(define (cross-x a b)
  (let ((a1 (gvector:x a))
	(a2 (gvector:y a))
	(a3 (gvector:z a))
	(b1 (gvector:x b))
	(b2 (gvector:y b))
	(b3 (gvector:z b)))
    (gvector (- (* a2 b3) (* a3 b2)) 
	     (- (* a3 b1) (* a1 b3)) 
	     (- (* a1 b2) (* a2 b1)))))

;; Calculates scalar product of two gvectors
(define ( scalar-x a b)
  (let ((a1 (gvector:x a))
	(a2 (gvector:y a))
	(a3 (gvector:z a))
	(b1 (gvector:x b))
	(b2 (gvector:y b))
	(b3 (gvector:z b)))
    (+ (* a1 b1) (* a2 b2) (* a3 b3))))

;;  Converts an edge to gvector
(define (vedge e)
  (let* ((verts (entity:vertices e))
	 (v1 (vertex:position (car verts)))
	 (v2 (vertex:position (car (cdr verts))))) 
    (gvector (- (position:x v2) (position:x v1)) 
	     (- (position:y v2) (position:y v1)) 
	     (- (position:z v2) (position:z v1)))))

;; Returns normal of face as gvector
(define (normal face)
  (let ((el (entity:edges face)))
    (cross-x (vedge (car el)) 
	     (vedge (car (cdr el))))))

;; Returns #t if two faces are perpendicular
(define (perp-faces f1 f2)
  (eq? (scalar-x (face:plane-normal f1)
		 (face:plane-normal f2)) 0))

;; Returns #t if the face is perpendicular to a gvector
(define (perp v f)
  (eq? (scalar-x (face:plane-normal f) v) 0))

;; Returns #t if face is on the domain boundary
(define (on-dom-boundary f)
  (let* ((face_center (cdr (list-ref (face:prop f) 2)))
	 (bbox-device (bbox (get-body-list)))
	 (maxx (position:x (cdr bbox-device)))
	 (minx (position:x (car bbox-device)))
	 (maxy (position:y (cdr bbox-device)))
	 (miny (position:y (car bbox-device)))
	 (eps 0.0001))
    (cond ((< (abs (- (position:x face_center) minx)) eps))
	  ((< (abs (- (position:x face_center) maxx)) eps))
	  ((< (abs (- (position:y face_center) miny)) eps))
	  ((< (abs (- (position:y face_center) maxy)) eps)#t)
	  (else #f))))

;; tapers a face around its top edge and vertical draft plane
(define (taper-face f angle)
  (let*((face_center (cdr (list-ref (face:prop f) 2)))
	(tp (position (position:x face_center) 
		      (position:y face_center)
		      (position:z (cdr (entity:box f))))))
    (sdegeo:taper-faces (list f) tp (gvector 0 0 1) angle)))

;; tapers faces in a list
(define (taper-faces-list L angle)
  (map (lambda(f) (taper-face f angle)) L))

(define (taper-faces ref angle body_id)
  (let*((all_faces (entity:faces body_id))
	;; filter out all faces that are NOT perpendicular to the ref   
	(perpfaces (filter-list all_faces (lambda(f)(perp ref f)) #f)))
    ;; filter out all faces that are ON domain boundary   
    (set! perpfaces (filter-list perpfaces (lambda(f)(on-dom-boundary f)) #t))
    (taper-faces-list perpfaces angle)))
