#math coord.ucs
#set mipY 0
#set bulkW 130
#set bulkD 100

AdvancedCalibration 2016.12

init Adaptive tdr=n@previous@_msh.tdr

pdbSet Silicon Phosphorus ActiveModel None
pdbSet Silicon Boron ActiveModel None

pdbSet ImplantData LeftBoundary Reflect
pdbSet ImplantData RightBoundary Reflect

pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error     1e37
pdbSet Grid AdaptiveField Refine.Rel.Error     1e10
pdbSet Grid AdaptiveField Refine.Target.Length 10.0
pdbSet Grid SnMesh UseLines 1
pdbSet Grid SnMesh DelaunayType boxmethod

math numThreads=16
refinebox clear

refinebox min= {0 0 0} max= {130 40 100} refine.fields= { NetActive Boron Phosphorus} refine.min.edge = {0.05 0.05 0.05} refine.max.edge = {50 50 50}  def.max.asinhdiff= 0.5

grid remesh

struct tdr=n@node@

exit
