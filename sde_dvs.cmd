
(load "SDE-scheme-utilities.scm")
(load "sdeoffsetmask.scm")


(sde:clear)

#(define GDSFILE "ATLASPixSimplified.gds")
#(define CELLNAME "?")
#(define LAYERNAMES (list 'RX 'DN 'NW 'BP 'CA 'M1 ))
#(define LAYERNUMBERS (list '0:0 '1:0 '2:0 '4:0 '5:0 '6:0 ))

#(sdeicwb:gds2mac "gds.file" GDSFILE "cell" CELLNAME "layer.names" LAYERNAMES "layer.numbers" LAYERNUMBERS "sim3d" (list 0 -6000 6000 0) "scale" 1.0e-3 "domain.name" "SIM3D" "mac.file" "ATLASPixSimple" "recenter" #t)

(sdeicwb:load-file "ATLASPix.mac" 0.001 #t)

(define LNames (sdeicwb:get-layer-names))
(define LIDs (sdeicwb:get-layer-ids))

(sdeicwb:set-domain "SIM3D")

#(sdeicwb:generate-mask-by-layer-name "CA" "CA")
(sdeicwb:generate-mask-by-layer-name "M1" "M1")
(sdeicwb:generate-mask-by-layer-name "NW" "NW")
(sdeicwb:generate-mask-by-layer-name "DN" "DN")
(sdeicwb:generate-mask-by-layer-name "BP" "BP")
(sdeicwb:generate-mask-by-layer-name "HV" "HV")
(sdeicwb:generate-mask-by-layer-name "VDDD" "VDDD")
(sdeicwb:generate-mask-by-layer-name "RX" "RX")


(define TSUB 100.0)

(sdepe:define-pe-domain (list 0.0 0.0 130.0 40.0))
(sdepe:add-substrate "material" "Silicon" "thickness" TSUB)

(sdedr:define-constant-profile "CP.substrate" "BoronActiveConcentration" @SubstrateNeff@)
(sdedr:define-constant-profile-material "placeCP.substrate" "CP.substrate" "Silicon")


(sdedr:define-gaussian-profile "DNWELL" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" 1e17 "ValueAtDepth" 1e12 "Depth" 6 "Gauss"  "Factor" 0.8)
(sdepe:pattern "mask" "DN" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )
(sdepe:implant "DNWELL")

(entity:delete (find-material-id "Resist"))

(sdedr:define-gaussian-profile "PWELL" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e18 "ValueAtDepth" 1e16 "Depth" 1 "Gauss"  "Factor" 0.8)
(sdepe:pattern "mask" "BP" "polarity" "dark" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )
(sdepe:implant "PWELL")


(entity:delete (find-material-id "Resist"))


(sdepe:icon_layer "mask" "RX" "polarity" "light" "ic-material" "Aluminum" "thickness" 0.8  "env-material" "Oxide" "taper-angle" -10)
(sdepe:icon_layer "mask" "M1" "polarity" "light" "ic-material" "Aluminum" "thickness" 0.45 "env-material" "Oxide" "taper-angle" -10)

(sdegeo:define-contact-set "HV" 4 (color:rgb 1 0 0) "##") 

(sdeicwb:contact  "layer.name" "HV" "type" "box" "material" "Aluminum")
(sdeicwb:contact  "layer.name" "VDDD" "type" "box" "material" "Aluminum")
(sdegeo:set-current-contact-set "HV") 
(sdegeo:define-3d-contact (find-face-id (position 65 20 0)) (sdegeo:get-current-contact-set))

#(sdeio:save-tdr-bnd (get-body-list) "n@node@_bnd.tdr")
	


; --- meshing ---

(sdedr:define-refinement-size "rf.substrate" 5 5 5 0.2 0.2 0.2 )
(sdedr:define-refinement-material "placeRF.substrate" "rf.substrate" "Silicon" )
(sdedr:define-refinement-function "rf.substrate" "DopingConcentration" "MaxTransDiff" 1)

(sde:set-meshing-command "snmesh -a -c boxmethod")
(sdedr:append-cmd-file "")
(sdesnmesh:set-iocontrols "numThreads" 8 )
(sde:build-mesh "snmesh" "-a -c boxmethod" "@pwd@/n@node@")
(sde:save-model "@pwd@/n@node@")



