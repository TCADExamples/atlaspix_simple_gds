source /eda/synopsys/2016-17/RHELx86/SENTAURUS_2016.12/tcad/current/lib/procem-26.0.1/sde-tcl2scm.tcl


(load "SDE-scheme-utilities.scm")
(load "sdeoffsetmask.scm")


(sde:clear)

#(define GDSFILE "ATLASPixSimplified.gds")
#(define CELLNAME "?")
#(define LAYERNAMES (list 'RX 'DN 'NW 'BP 'CA 'M1 ))
#(define LAYERNUMBERS (list '0:0 '1:0 '2:0 '4:0 '5:0 '6:0 ))

#(sdeicwb:gds2mac "gds.file" GDSFILE "cell" CELLNAME "layer.names" LAYERNAMES "layer.numbers" LAYERNUMBERS "sim3d" (list 0 -6000 6000 0) "scale" 1.0e-3 "domain.name" "SIM3D" "mac.file" "ATLASPixSimple" "recenter" #t)

(sdeicwb:load-file "ATLASPix.lyt" 1 #t)

(define LNames (sdeicwb:get-layer-names))
(define LIDs (sdeicwb:get-layer-ids))

(sdeicwb:set-domain "SIM3D")

(sdeicwb:generate-mask-by-layer-name "CA" "CA")

(define TSUB 100.0)

(sdepe:define-pe-domain (list -20.0 -75.0 20.0 75.0))

(sdepe:add-substrate "material" "Silicon" "thickness" TSUB)
(sdepe:depo "material" "Oxide" "thickness" 0.1)

(sdepe:pattern "mask" "CA" "polarity" "light" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )

(sdepe:etch-material "material" "Oxide" "depth" 0.1 ) 

(entity:delete (find-material-id "Resist"))


(sdeio:save-tdr-bnd (get-body-list) "n@node@_bnd.tdr")
	




