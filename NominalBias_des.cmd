#setdep @node|STRUCTURE@


File {
    Grid       = "n@node|STRUCTURE@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    output = "@log@"
    parameter = "@parameter@"
}



Electrode {
	{name = "HV"   voltage = 0.0     voltage=0 	eRecVelocity=1e7 hRecVelocity=1e7 }	
	{name = "VDDD"   voltage = 0.0     voltage=0	eRecVelocity=1e7 hRecVelocity=1e7 }			
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)
} 





CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
RadiationGeneration(Maximum(material="Silicon"))
}

Math {


  Derivatives
  Avalderivatives
  Digits=5
  Notdamped=1000
  Iterations=8
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)

  Transient=BE
  
 Method=Pardiso
  
  ILSrc = "
           set(1) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(0.0001,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };
           
           set(2) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(1e-8,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };

           
     "

  number_of_threads=6

 Extrapolate 
}


Solve {

  Poisson
  plugin { Poisson Electron Hole }
  Coupled { Poisson Electron Hole }

 NewCurrent="Nominal_"    
                 
    Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=1e-8 InitialStep=1e-5
			Increment=3 Decrement=4.0
                  		Goal { Name="HV" Voltage=-@Bias@ } 
                 		)
                  { Coupled {  Poisson Electron Hole } } 
}
   

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime
 	
 	RadiationGeneration


}



